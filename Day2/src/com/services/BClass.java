/*
 *class named as �A� and create a sub class �B�. Which is extends from class �A�. 
 *And use these classes in �inherit� class.  
 * 
 */package com.services;

public class BClass extends AClass {
	public BClass() {
		super();
		System.out.println("I am B Class");
	}

	public String classBMethod() {
		return "I am derive Class";
	}

}
