/*you must add a �bark method to the Dog class, then modify the main method 
 * accordingly so that the code prints the following lines: 
 *I am walking 
 *I am eating 
 *I am barking 
 **/

package com.services;

public class Dog extends Animal {
	public void eat() {

		System.out.println("I am eating");

	}

	public void walk() {
		super.walk();
	}

	public void bark() {
		System.out.println("I am barking");
	}

}
