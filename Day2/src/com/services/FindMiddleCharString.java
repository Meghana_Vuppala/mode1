/*
 * display the middle character of a string.  
 * 
 * */
package com.services;

public class FindMiddleCharString {
	public String findMiddleCharString(String inputString) {
		int reminder = inputString.length() % 2;
		int value = inputString.length() / 2;
		if (reminder == 0) {
			return inputString.charAt(value - 1) + " " + inputString.charAt(value);
		} else {
			return inputString.charAt(value) + " ";
		}

	}

}
