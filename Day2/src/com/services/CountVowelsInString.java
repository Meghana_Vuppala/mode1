/*
 *  count all vowels in a string 
 * 
 * */
package com.services;

public class CountVowelsInString {
	public int countVowelsInString(String inputString) {
		int i = 0, count = 0;
		while (i < inputString.length()) {
			if (inputString.charAt(i) == 'a' || inputString.charAt(i) == 'e' || inputString.charAt(i) == 'i'
					|| inputString.charAt(i) == 'o' || inputString.charAt(i) == 'u') {
				count++;
			}
			i++;
		}
		return count;
	}

}
