/*
 * smallest number among three numbers
 * 
 * */
package com.services;

public class FindSmallestNumber {
	public int findSmallestNumber(int firstValue, int secondValue, int thirdValue) {
		int value;
		value = (firstValue <= secondValue) ? ((firstValue <= thirdValue) ? (firstValue) : (thirdValue))
				: ((secondValue <= thirdValue) ? (secondValue) : (thirdValue));
		return value;

	}

}
