/*
 * create the class Addition and the required methods so that the code prints the 
 * sum of the numbers passed to the function addition. 
 * 
 * */
package com.services;

public class AddtionClass {
	public int addMethod(int... integerValue) {
		int sum = 0;
		for (int i : integerValue) {
			sum += i;
		}
		return sum;
	}

}
