/*
 * create a room class, the attributes of this class is roomno, roomtype, roomarea and ACmachine. 
 * In this class the member functions are setdata and displaydata
 * */
package com.pojo;

public class RoomClassPojo {
	private int roomNo;
	private String roomType;
	private int roomArea;
	private String acMachine;

	public void displayData() {
		System.out.println("RoomNo=" + getRoomNo() + ", RoomType=" + getRoomType() + ", RoomArea=" + getRoomArea()
				+ ", AcMachine=" + getAcMachine());
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		if (roomType == "AC")
			this.roomType = roomType;
		else
			this.roomType = roomType;
	}

	public int getRoomArea() {
		return roomArea;
	}

	public void setRoomArea(int length, int breath) {
		this.roomArea = length * breath;
		;
	}

	public String getAcMachine() {
		return acMachine;
	}

	public void setAcMachine(String acMachine) {
		this.acMachine = acMachine;
	}

}
