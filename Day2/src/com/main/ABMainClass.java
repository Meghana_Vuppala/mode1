/*
 *class named as �A� and create a sub class �B�. Which is extends from class �A�. 
 *And use these classes in �inherit� class.  
 * 
 */package com.main;

import com.services.AClass;
import com.services.BClass;

public class ABMainClass {

	public static void main(String[] args) {
		AClass aObject = new AClass();
		BClass bObject = new BClass();
		System.out.println(bObject.classBMethod());
	}

}
