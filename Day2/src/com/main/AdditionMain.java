/*
 * create the class Addition and the required methods so that the code prints the 
 * sum of the numbers passed to the function addition. 
 * 
 * */
package com.main;

import com.services.AddtionClass;

public class AdditionMain {

	public static void main(String[] args) {
		AddtionClass object = new AddtionClass();
		int result;
		result = object.addMethod(25, 25, 25, 25, 25, 25);
		System.out.println(result);
	}

}
