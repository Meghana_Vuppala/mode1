/*
 * smallest number among three numbers
 * 
 * */

package com.main;

import java.util.Scanner;
import com.services.FindSmallestNumber;

public class FindSmallestNumberMain {

	public static void main(String[] args) {
		System.out.println("Enter three values");
		Scanner input = new Scanner(System.in);
		int value = 0;
		int firstValue = input.nextInt();
		int secondValue = input.nextInt();
		int thirdValue = input.nextInt();
		FindSmallestNumber object = new FindSmallestNumber();
		value = object.findSmallestNumber(firstValue, secondValue, thirdValue);
		System.out.println("smallest number is :" + value);
		input.close();
	}

}
