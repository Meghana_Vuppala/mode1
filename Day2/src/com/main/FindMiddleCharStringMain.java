/*
 * display the middle character of a string.  
 * 
 * */
package com.main;

import java.util.Scanner;

import com.services.FindMiddleCharString;

public class FindMiddleCharStringMain {

	public static void main(String[] args) {
		System.out.println("Enter String:");
		Scanner input = new Scanner(System.in);
		String inputString = input.nextLine();
		FindMiddleCharString inpStr = new FindMiddleCharString();
		String result = inpStr.findMiddleCharString(inputString);
		System.out.println("The middle character in the string:" + result);
		input.close();
	}

}
