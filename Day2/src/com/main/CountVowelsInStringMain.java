/*
 *  count all vowels in a string 
 * 
 * */
package com.main;

import java.util.Scanner;

import com.services.CountVowelsInString;

public class CountVowelsInStringMain {

	public static void main(String[] args) {
		System.out.println("Enter String:");
		Scanner input = new Scanner(System.in);
		String inputString = input.nextLine();
		CountVowelsInString inpStr = new CountVowelsInString();
		int count = inpStr.countVowelsInString(inputString);

		System.out.println("Number of  Vowels in the string :" + count);
		input.close();
	}

}
