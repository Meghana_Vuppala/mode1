/*
 * create a room class, the attributes of this class is roomno, roomtype, roomarea and ACmachine. 
 * In this class the member functions are setdata and displaydata
 * */
package com.main;

import com.pojo.RoomClassPojo;

public class RoomClassMain {

	public static void main(String[] args) {
		RoomClassPojo rcp = new RoomClassPojo();
		rcp.setRoomNo(111);
		rcp.getRoomNo();
		rcp.setRoomType("Normal");
		rcp.getRoomType();
		rcp.setRoomArea(50, 20);
		rcp.getRoomArea();
		rcp.setAcMachine("AC");
		rcp.getAcMachine();
		rcp.displayData();
	}

}
