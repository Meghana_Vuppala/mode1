/* written By @Meghana
 * 
 * Return the String without the first 2 characters except when 
 *Keep the first char if it is 'k' 
 *Keep the second char if it is 'b'. 
 * */
package com.hcl;

public class UserMainCodeRemoveFirstTwoChar {
	public static StringBuffer getString(StringBuffer string) {
		StringBuffer tempString = null;

		if (string.charAt(0) != 'k' && string.charAt(1) != 'b') {
			tempString = string.delete(0, 2);
		} else if (string.charAt(0) != 'k' && string.charAt(1) == 'b') {
			tempString = string.deleteCharAt(0);
		} else if (string.charAt(0) == 'k' && string.charAt(1) != 'b') {
			tempString = string.deleteCharAt(1);
		} else
			tempString = string;

		return tempString;
	}
}
