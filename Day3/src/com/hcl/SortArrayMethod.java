/*Written BY @meghana
 * 
 * sorting of an ArrayElements
 * */
package com.hcl;

public class SortArrayMethod {
	public static int[] mergeArray(int array[]) {
		Arrays.sort(array);
		return array;
}
