/*
 * written by @Meghana
 * */

package com.main;

import java.util.Scanner;

import com.hcl.Calculator;

public class CalculatorMain {

	public static void main(String[] args) {
		System.out.println("Enter two integers");
		Scanner input = new Scanner(System.in);
		int firstValue=input.nextInt();
		int secondValue=input.nextInt();
		Calculator object=new Calculator();
		System.out.println(object.add(firstValue,secondValue));
		input.close();

	}

}
