/*  written By @Meghana
 * 
 *Given a string , print �Yes� if it is
 * a palindrome, print �No� otherwise.   
 * */
package com.main;

import java.util.Scanner;

public class Stringpalindrome {

	public static void main(String[] args) {
		System.out.println("Enter String:");
		Scanner input = new Scanner(System.in);
		String string = input.nextLine();
		int i = 0;
		int flage = 0;
		int strLength = string.length();
		int middleValue = strLength / 2;
		while (i < middleValue) {
			if (string.charAt(i) != string.charAt(strLength - 1 - i)) {
				flage = 1;
				break;
			}
			i++;
		}
		if (flage == 1)
			System.out.println("No");
		else
			System.out.println("Yes");
		input.close();
	}

}
