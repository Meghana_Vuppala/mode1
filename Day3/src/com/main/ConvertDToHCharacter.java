/*Written By @Meghana
 * 
 * program to replace all the 'd' occurrence characters with �h� characters in each string. 
 * 
 * */
package com.main;

import java.util.Scanner;

public class ConvertDToHCharacter {

	public static void main(String[] args) {
		System.out.println("Enter String");
		Scanner input = new Scanner(System.in);
		String string = input.nextLine();
		String repalceString = string.replace('d', 'h');
		System.out.println(repalceString);
		input.close();
	}

}
