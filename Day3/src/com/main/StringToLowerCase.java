/* written By @Meghana
 *convert all the characters in a string to lowercase. 
 * */
package com.main;

import java.util.Scanner;

public class StringToLowerCase {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String string = input.nextLine();
		System.out.println(string.toLowerCase());
		input.close();
	}
}
