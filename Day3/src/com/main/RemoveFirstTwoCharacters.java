/* written By @Meghana
 * 
 * Return the String without the first 2 characters except when 
 *Keep the first char if it is 'k' 
 *Keep the second char if it is 'b'. 
 * */

package com.main;

import java.util.Scanner;

import com.hcl.UserMainCodeRemoveFirstTwoChar;

public class RemoveFirstTwoCharacters {

	public static void main(String[] args) {
		System.out.println("Enter String:");
		Scanner input=new Scanner(System.in);
		String string = input.nextLine();
		StringBuffer string1 = new StringBuffer(string);
		System.out.println(UserMainCodeRemoveFirstTwoChar.getString(string1));
		input.close();
	}

}
