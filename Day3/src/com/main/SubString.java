/* written By @Meghana
 * Accept a string, and two indices(integers), and print the substring 
 * consisting of all characters inclusive range from ..to .  
 * */

package com.main;

import java.util.Scanner;

public class SubString {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String string = input.nextLine();
		System.out.println("Enter lower limit and upper limit");
		int lowerlimit = input.nextInt();
		int upperlimit = input.nextInt();
		if (lowerlimit <= upperlimit) {
			if (lowerlimit > -1 && upperlimit < string.length()) {
				int i = lowerlimit;
				while (i <= upperlimit) {
					System.out.println(string.charAt(i));
					i++;
				}
			}
		} else if (upperlimit < lowerlimit) {
			if (upperlimit > -1 && lowerlimit < string.length()) {
				int i = upperlimit;
				while (i <= lowerlimit) {
					System.out.println(string.charAt(i));
					i++;
				}
			}
		} else
			System.out.println("Enter valid lowerlimit and upperlimit");
		input.close();
	}

}
