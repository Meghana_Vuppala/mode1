/*written By @Meghana
 * 
 * read a string and a character, and reverse the string and convert
 *  it in a format such that each character is separated by the given character. 
 *  read a string and a character, and reverse the string and convert it in a format such that each character is separated by the given character. Print the final string. Print the final string. 
 * 
 * */

package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode;

public class ReshapeMain {

	public static void main(String[] args) {
		System.out.println("Enter String:");
		Scanner input = new Scanner(System.in);
		String string = input.nextLine();
		StringBuffer string1 = new StringBuffer(string);
		System.out.println(UserMainCode.reshape(string1.reverse()));
		input.close();
	}

}
