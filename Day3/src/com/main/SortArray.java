/*Written By @Meghana
 * 
 *Sorting an array 
 * */
package com.main;

import java.util.Scanner;

import com.hcl.SortArrayMethod;

public class SortArray {

	public static void main(String[] args) {
		System.out.println("Enter the size of ARRAY");
		Scanner input = new Scanner(System.in);
		int size = input.nextInt();
		int elementArray[] = new int[size];
		System.out.println("Enter the values:");
		for (int i = 0; i < size; i++) {
			elementArray[i] = input.nextInt();
		}
		int[] resultArray=SortArrayMethod.mergeArray(elementArray);
		for(int i=0;i<resultArray.length;i++) {
			System.out.println(resultArray[i]);
			}
		input.close();
	}

}
