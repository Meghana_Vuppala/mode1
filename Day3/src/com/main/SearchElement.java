/*Written By @Meghana
 * 
 *Searching an array Elements 
 * */
package com.main;

import java.util.Arrays;
import java.util.Scanner;

public class SearchElement {

	public static void main(String[] args) {
		System.out.println("Enter the size of Array");
		Scanner input = new Scanner(System.in);
		int size = input.nextInt();
		int[] array = new int[size];
		System.out.println("Enter the Array Elements");
		for (int i = 0; i < size; i++) {
			array[i] = input.nextInt();
		}
		System.out.println("Enter the search element");
		int searchElement = input.nextInt();
		Arrays.sort(array);
		int index = Arrays.binarySearch(array, searchElement);
		if (index > -1)
			System.out.println("element found is " + array[index]);
		else
			System.out.println("Element not found");
		input.close();
	}

}
