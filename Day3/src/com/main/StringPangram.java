/*written By @Meghana
 *  find the given string is pangram or not.  
 * */


package com.main;

import java.util.Scanner;

public class StringPangram {

	public static void main(String[] args) {
		System.out.println("Enter String:");
		Scanner input = new Scanner(System.in);
		String string = input.nextLine();
		int i = 0;
		int value = 0;
		int flage = 0;
		int count[] = new int[26];
		while (i < string.length()) {
			if (string.charAt(i) >= 65 && string.charAt(i) <= 90)
				value = string.charAt(i) - 'A';
			else if (string.charAt(i) >= 97 && string.charAt(i) <= 122)
				value = string.charAt(i) - 'a';

			count[value] = 1;
			i++;
		}
		for (int j = 0; j < 26; j++) {
			if (count[j] == 0) {
				flage = 1;
				break;
			}
		}
		if (flage == 0)
			System.out.println("Pangram");
		else
			System.out.println("Not Pangram");

		input.close();
	}

}
