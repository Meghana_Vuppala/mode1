/*
 * calculate the sum of odd digits (values) 
 * present in the given number. 
 * */
package com.services;

public class UserMainCode {
	public static int checkSum(String s) {
		int sum = 0;
		for (int i = 1; i < s.length(); i = i + 2) {
			sum += Integer.parseInt(s.substring(i, i + 1));
			// System.out.println(s.substring(i,i+1));
			// System.out.println("sum value:"+sum);
		}
		if (sum % 2 == 1)
			return 1;
		else
			return -1;
	}
}
