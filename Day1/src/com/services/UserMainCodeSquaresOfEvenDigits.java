/*calculate the sum of squares of even digits (values) 
 * present in the given number.  
 * 
 * */
package com.services;

public class UserMainCodeSquaresOfEvenDigits {
	public static int sumOfSquaresOfEvenDigits(String s) {
		int res = 0, a;
		for (int i = 0; i < s.length(); i = i + 2) {
			a = Integer.parseInt(s.substring(i, i + 1));
			res += (a * a);
		}
		return res;
	}
}
