/*Your program should read a sentence as input 
 * from user and return the longest word. In case 
 * there are two words of maximum length return the 
 * word which comes first in the sentence. 
 * 
 * */
package com.services;

import java.util.Scanner;

public class UserMainCodeLargestWord {
	public static String getLargestWord(Scanner sc) {
		String a;
		String word = " ";
		String inputWord[];
		int wordLength;
		int length = 0;
		inputWord = sc.nextLine().split(" ");
		for (int i = 0; i < inputWord.length; i++) {
			a = inputWord[i];
			wordLength = a.length();
			if (wordLength > length) {
				length = wordLength;
				word = a;
			}
		}
		return word;
	}
}
