/*
 *  number variable and check whether the number is prime or not. 
 * */
package com.main;

import java.util.Scanner;

public class PrimeNumber {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Integer");
		int a = sc.nextInt();
		int flag = 1;
		for (int i = 2; i < a; i++) {
			if (a % i == 0) {
				flag = 0;
				break;
			} else
				continue;
		}
		if (flag == 1)
			System.out.println(a + " is prime Number");
		else
			System.out.println(a + " is not a prime Number");
		sc.close();
	}
}
