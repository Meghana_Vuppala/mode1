package com.main;

/*Your program should read a sentence as input from user 
 * and return the longest word. In case there are two words 
 * of maximum length return the word which comes first in the sentence. 
 * */
import java.util.Scanner;

import com.services.UserMainCodeLargestWord;

public class LargestWord {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string");
		String longStr;
		longStr = UserMainCodeLargestWord.getLargestWord(sc);
		System.out.println(longStr);

	}

}
