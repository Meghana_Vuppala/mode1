/*calculate the sum of odd digits (values) 
 * present in the given number. 
 * 
 * */
package com.main;

import java.util.Scanner;

import com.services.UserMainCode;

public class SumOfOddDigits {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Integer");
		int a=sc.nextInt();
		int res=0;
		if(a>=0) {
			String s=Integer.toString(a);
			res=UserMainCode.checkSum(s);
			}
		else
			System.out.println("Integer is Negative Value");	
		
		if(res==1)	
			System.out.println("Sum of odd digits is odd");	
		else if(res==-1)
			System.out.println("Sum of odd digits is even");
		sc.close();

	}

}
