/*
 * print the ascii value of a given character.  
 * */
package com.main;

import java.util.Scanner;

public class CharToAscii {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter character");
		char a = sc.next().charAt(0);
		int ascii = (int) a;
		System.out.println("ascii value:" + ascii);
		sc.close();
	}
}
