/*
 * calculate the sum of squares of even digits (values) 
 * present in the given number.  
 * */
package com.main;

import java.util.Scanner;

import com.services.UserMainCodeSquaresOfEvenDigits;

public class SumOfSquaresEvenDigits {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Integer:");
		int b = sc.nextInt();
		if (b >= 0) {
			String s = Integer.toString(b);
			int res = UserMainCodeSquaresOfEvenDigits.sumOfSquaresOfEvenDigits(s);
			System.out.println(res);
		} else
			System.out.println("Integer is Negative Value");
		sc.close();
	}

}
