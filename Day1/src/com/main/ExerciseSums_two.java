/*
 * Java program to print the sum (addition), multiply, subtract, divide and remainder of two numbers.  
 * */
package com.main;

import java.util.Scanner;

public class ExerciseSums_two {
	public static void main(String[] args) {
		System.out.println("Enter two integers");
		Scanner input =new Scanner(System.in);  
		int firstValue=input.nextInt();
		int secondValue=input.nextInt();
		System.out.println("addtion: "+(firstValue+secondValue)+" "+"subraction: "+(firstValue-secondValue)+" "+"multiply: "+(firstValue*secondValue)+" "+"division: "+(firstValue/secondValue)+"%: "+(firstValue%secondValue));
		input.close();
	}
}
