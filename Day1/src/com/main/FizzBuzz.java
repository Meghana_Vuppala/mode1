/*
 *  integers from 1 to 100. For multiples of three print "Fizz" instead of the number 
 *  and print "Buzz" for the multiples of five. When number is divided by both three 
 *  and five, print "fizz buzz". 
 * 
 * */
package com.main;

public class FizzBuzz {

	public static void main(String[] args) {
		String value;
		for (int i = 1; i <= 100; i++) {
			value = (i % 3 == 0) ? ((i % 5 == 0) ? ("Fizz Buzz") : ("Fizz")) : ((i % 5 == 0) ? ("Buzz") : (""));
			if ((value == "Fizz") || (value == "Buzz") || (value == "Fizz Buzz"))
				System.out.println(i + value);
		}
	}

}
