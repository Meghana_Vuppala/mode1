/*
 * takes three numbers as input to calculate and print 
 * the average of the numbers. 
 * */
package com.main;

import java.util.Scanner;

public class AverageSum {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		System.out.println((a + b + c) / 3);
		sc.close();
	}
}
