/*
 * program to swap two variables.  
 * */
package com.main;

import java.util.Scanner;

public class SwapSum {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the two integers");
		int a=sc.nextInt();
		int b=sc.nextInt();
		System.out.println("Before swaping values: "+"a:"+a+" b:"+b);
		a=a+b;
		b=a-b;
		a=a-b;
		System.out.println("After swaping values: "+"a:"+a+" b:"+b);
		sc.close();
	}
}
